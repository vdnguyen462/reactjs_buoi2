import React, { Component } from 'react'
import './ChooseGlasses.css'
export default class ChooseGlasses extends Component {
    state = {
        glassList:
        [
            {
                "id": 1,
                "price": 30,
                "name": "GUCCI G8850U",
                "url": "./glassesImage/v1.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 2,
                "price": 50,
                "name": "GUCCI G8759H",
                "url": "./glassesImage/v2.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 3,
                "price": 30,
                "name": "DIOR D6700HQ",
                "url": "./glassesImage/v3.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 4,
                "price": 70,
                "name": "DIOR D6005U",
                "url": "./glassesImage/v4.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 5,
                "price": 40,
                "name": "PRADA P8750",
                "url": "./glassesImage/v5.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 6,
                "price": 60,
                "name": "PRADA P9700",
                "url": "./glassesImage/v6.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 7,
                "price": 80,
                "name": "FENDI F8750",
                "url": "./glassesImage/v7.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 8,
                "price": 100,
                "name": "FENDI F8500",
                "url": "./glassesImage/v8.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
            {
                "id": 9,
                "price": 60,
                "name": "FENDI F4300",
                "url": "./glassesImage/v9.png",
                "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
            },
        ],
    };

    renderGlasses = () =>{
        let glassListComponent = this.state.glassList.map((glass) => {
            return(
                <div>
                    <img src={glass.url} onClick={() => {
                        this.glassSelected(glass.id);
                    }}/>
                </div>
            );
        });
        return glassListComponent;
    };
    glassSelected =  (id) => {
        let glass = this.state.glassList.filter((glass) => 
            glass.id == id
        );
        this.setState({
            imgGlass: glass[0].url,
            price: glass[0].price,
            name: glass[0].name,
            desc: glass[0].desc,
        });
    };
  render() {
    return (
      <div className='changeGlasses'>
        <div className="backgroundImage">
            <h2 className='text-center'>TRY GLASS APP ONLINE</h2>
            <img src="./glassesImage/background.jpg"/>
        </div>
        <div className='content p-5'>
            <div className="avatar">
                <div className="row">
                    <div className="col-6 image-demo">
                        <img src="./glassesImage/model.jpg" alt="" />
                    </div>
                    <div className='col-6 '>
                        <div className='image-model'>
                            <img src="./glassesImage/model.jpg" alt="" />
                        </div>
                        <div className="image-detail">
                            <div className="image-glass">
                                <img  src={this.state.imgGlass}/>
                            </div>
                            <div className='image-infor'>
                                <h3 className='display-5 font-weight-bold text-center'>{this.state.price}</h3>
                                <h4 className='text-center text-danger mx-6'>{this.state.name}</h4>
                                <p>{this.state.desc}</p>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
            <div className="glasses">
                <div className='row'>{this.renderGlasses()}</div>
            </div>
        </div>
      </div>
    )
  }
}
