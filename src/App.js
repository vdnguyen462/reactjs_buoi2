import logo from './logo.svg';
import './App.css';
import ChooseGlasses from './ChooseGlasses/ChooseGlasses';

function App() {
  return (
    <div>
      <ChooseGlasses/>
    </div>
  );
}

export default App;
